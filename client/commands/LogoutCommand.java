/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client.commands;

import client.CLI;
import client.Client;

import java.io.IOException;

/*
 * LogoutCommand
 */
public class LogoutCommand implements Command
{
	private Client client;
	private CLI cli;

	public LogoutCommand (Client client, CLI cli) {
		this.client = client;
		this.cli = cli;
	}

	@Override
	public void execute (String[] args)
	{
		try {
			this.client.logout();

			cli.removeCommand("submit");
			cli.removeCommand("financing");
			cli.removeCommand("listFin");
			cli.removeCommand("listEnd");
			cli.removeCommand("infoproj");
			cli.removeCommand("logout");
			cli.removeCommand("register");
			cli.removeCommand("deposit");
			cli.removeCommand("saldo");

			cli.addCommand("l", "login", "login to server", new LoginCommand (client, cli));
			cli.setPrompt("u-kickstarter>");

		} catch (IOException e) {
			System.out.println("Server connection lost");
		}
	}

	@Override
	public int n_args () {return 0;}
}
