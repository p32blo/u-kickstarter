/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client.commands;

import client.CLI;
import client.Client;

import java.io.IOException;

/*
 * LoginCommand
 */
public class LoginCommand implements Command
{
	private Client client;
	private CLI cli;

	public LoginCommand (Client client, CLI cli) {
		this.client = client;
		this.cli = cli;
	}

	@Override
	public void execute (String[] args)
	{
		try {
			String log = this.client.login(args[1], args[2]);

			// adicao de commandos para o utilizador
			switch (log) {
				case "ADMIN":
					cli.addCommand("reg", "register", "register user to server", new RegisterCommand (client));
					cli.setPrompt((args[1] + "@u-kickstarter> ").toUpperCase());
					break;
				case "OK":
					cli.addCommand("sub", "submit", "submit project", new SubmitCommand (client));
					cli.addCommand("fin", "finance", "finance project", new FinancingCommand (client));
					cli.addCommand("lf", "listFin", "project list financing", new ListFinancingCommand (client));
					cli.addCommand("le", "listEnd", "project list financed", new ListFinancedCommand (client));
					cli.addCommand("info", "infoproj", "list project info", new ProjectInfoCommand (client));
					cli.addCommand("dep", "deposit", "deposit money", new DepositCommand (client));
					cli.addCommand("bal", "saldo", "account balance", new SaldoCommand (client));
					cli.setPrompt(args[1] + "@u-kickstarter> ");
					break;
				default:
					// Se derro nao fazer nada
					return;
			}
			cli.addCommand("out", "logout", "logout from server", new LogoutCommand (client, this.cli));
			cli.removeCommand("login");

		} catch (IOException e) {
			System.out.println("Server connection lost");
		}
	}

	@Override
	public int n_args () {return 2;}
}
