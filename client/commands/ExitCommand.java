/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client.commands;

/*
 * ExitCommand
 */
public class ExitCommand implements Command
{
	@Override
	public void execute (String[] args) {
		System.exit(0);
	}

	@Override
	public int n_args () {return 0;}
}
