/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client.commands;

/*
 * Command - Interface para a execucao de comandos pelo CLI
 */
public interface Command
{
	/* Metodo a invocar aquando da invocacao */
	public void execute (String[] args);

	/* Nuemero de argumentos que o comando necessita */
	public int n_args ();
}
