/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client.commands;

import client.Client;
import java.io.IOException;

/*
 * SubmitCommand
 */
public class SubmitCommand implements Command
{
	private Client client;

	public SubmitCommand (Client client) {
		this.client = client;
	}

	@Override
	public void execute (String[] args)
	{
		try {
			this.client.submit(args[1], args[3], args[2]);
		} catch (IOException e) {
			System.out.println("Server connection lost");
		}
	}

	@Override
	public int n_args () {return 3;}
}
