/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client;
import client.commands.*;

import java.io.Console;

import java.util.List;
import java.util.ArrayList;

import java.util.Map;
import java.util.HashMap;


/*
 * CLI - Command-Line Interface
 *
 * Interface de comandos para o utilizador interagir
 * com o programa atravez da linha de comandos.
 *
 * NOTA: A todo o momento deve existir um commando que estaja identificado como de saida.
 */
public class CLI
{
	// prompt da linha de comandos
	private String prompt;
	// Guardar o comando que vai sair
	private CommandDescription quit;

	// Descricoes dos commandos para a ajuda
	private List<CommandDescription> commands;
	// Comando lido da linha de comandos -> comando a executar
	private Map<String, Command> call_cmds;

	/* A prompt por defeito e '> ' */
	public CLI () {
		this("> ");
	}

	/* Inicializacao das variaveis*/
	public CLI (String prompt)
	{
		this.prompt = prompt;

		// O comando de saida nao existente
		this.quit = null;

		// commandos sao enseridos atravez de metodos nesta classe
		this.commands = new ArrayList<CommandDescription> ();
		this.call_cmds = new HashMap<String, Command> ();
	}

	/* Change CLI prompt*/
	public void setPrompt (String prompt) {	this.prompt = prompt;}

 	/* Output de uma linha no menu de ajuda */
	private void help_line (String abrev, String name, String description, int n_args) {
		System.out.println("\t" + abrev + " : " + name + "\t- " + description + " (" + n_args  +" args)");
	}

	/* Output de um CommandDescription no menu de ajuda */
	private void help_line (CommandDescription cmd) {
		this.help_line(cmd.abrev, cmd.name, cmd.description, cmd.n_args);
	}

	/* Quando nao e dado os n_args */
	private void help_line (String abrev, String name, String description) {
		this.help_line(abrev, name, description, 0);
	}

	/* Output do menu de ajuda */
	public void help ()
	{
		System.out.println("Usar: <cmd> <opts>");
		System.out.println("Lista de comandos:");

		for (CommandDescription cmd : this.commands) {
			help_line(cmd);
		}

		help_line("h", "help", "Devolve esta lista");
	}

	/* Adicionar um comando a ser processado pela CLI */
	public void addCommand (String abrev, String name, String description, Command cmd) {
		this.addCommand(abrev, name, description, cmd, false);
	}

	/*
	 * Adicionar o comando de saida da CLI.
	 * Isto serve para permitir a personalizacao da CLI aquando da saida da mesma.
	 */
	public void addCommand (String abrev, String name, String description, Command cmd, boolean end)
	{
		// Se o comando ja existir nao inserir
		if (this.call_cmds.containsKey(name)) {
			return;
		}

		// se for um comando identificado com de saida
		if (end) {
			this.quit = new CommandDescription (abrev, name, description, cmd.n_args());
		}

		// inserir tanto para a o comando como para a sua abreviatura
		this.call_cmds.put(abrev, cmd);
		this.call_cmds.put(name, cmd);

		this.commands.add(new CommandDescription (abrev, name, description, cmd.n_args()));
	}

	/* Remover um comando da CLI */
	public void removeCommand (String name)
	{
		CommandDescription com = new CommandDescription ();

		// Procurar por um comando com o nome dado
		for (CommandDescription cd : this.commands) {
			if (cd.name.equals(name)) {
				com = cd;
				break;
			}
		}

		// se nao existir nao fazer nada
		this.call_cmds.remove(com.abrev);
		this.call_cmds.remove(com.name);

		this.commands.remove(com);
	}

	/* Retirar '"' de args  */
	private void cleanArgs (String [] args)
	{
		for (int i = 0; i < args.length; i++) {
			if (args[i].contains("\"")){
				args[i] = args[i].substring(1, args[i].length()-1);
			}
		}
	}

	/*
	 * CLI Main loop
	 */
	public void start ()
	{
		Console console = System.console();

		// se nao existir um comando identificado como de saida
		if (this.quit == null) {
			System.out.println("usar a flag 'end' num e num so metodo em CLI.addCommand()");
			return;
		}

		String line;
		String cmd_name = "";

		// Enquanto o comando nao for o de saida
		while (!(cmd_name.equals(this.quit.abrev) || cmd_name.equals(this.quit.name)))
		{
			// Ler linha
			line = console.readLine(this.prompt).trim();

			// se estiver vazia
			if (line.isEmpty()) {
				continue;
			}

			// dividir uma linha por espacos ou palavras envolvidas por aspas
			String[] args = line.split("[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

			this.cleanArgs(args);

			cmd_name = args[0];

			Command cmd = this.call_cmds.get(cmd_name);

			// se encontrar o comando
			if (cmd != null) {
				// Se tiver o numero de argumentos certos
				if (cmd.n_args() == args.length - 1) {
					cmd.execute(args);
				} else {
					System.out.println("Invalid args");
				}
			}
			// se for o comando de ajuda
			else if (cmd_name.equals("help") || cmd_name.equals("h")) {
				this.help();
			}
			// caso invalido
			else {
				System.out.println("Not a valid Command.");
			}

			System.out.println();
		}
	}
}


/*
 * Class auxiliar para guarda a descricao de um comando
 */
class CommandDescription
{
	String abrev;
	String name;
	String description;
	int n_args;

	CommandDescription () {
		this("", "", "");
	}

	CommandDescription (String abrev, String name, String description)
	{
		this(abrev, name, description, 0);
	}

	CommandDescription (String abrev, String name, String description, int n_args)
	{
		this.abrev = abrev;
		this.name = name;
		this.description = description;
		this.n_args = n_args;
	}
}
