/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client;
import client.commands.*;

import java.io.IOException;

/*
 * Main
 */
public class Main
{
	public static void main (String[] args)
	{
		try {
			CLI cli = new CLI("u-kickstarter> ");
			Client client = new Client();

			// Mensagem inicial
			System.out.println("Bem-Vindo ao micro-Kickstarter");
			System.out.println("Para proceder faca login...");
			System.out.println();

			cli.addCommand("l", "login", "login to server", new LoginCommand (client, cli));
			cli.addCommand("q", "quit", "quit the program", new ExitCommand(), true);
			cli.help();
			System.out.println();

			cli.start();
		} catch (IOException e) {
			System.out.println("O servidor nao esta disponivel");
		}
	}
}
