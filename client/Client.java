/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package client;

import java.net.Socket;
import java.net.UnknownHostException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.IOException;

/*
 * Client
 */
public class Client
{
	Socket cs;

	PrintWriter out;
	BufferedReader in;

	/* O construtor inicia o contacto com o servidor */
	public Client ()
		throws IOException
	{
		this.contactServer ();
	}

	/* Contactar o servidor */
	private void contactServer ()
		throws UnknownHostException, IOException
	{
		// contacta o servidor
		this.cs = new Socket ("127.0.0.1", 9999);

		// Obter as streams de input e output
		this.out = new PrintWriter (this.cs.getOutputStream(), true);
		this.in = new BufferedReader (new InputStreamReader (this.cs.getInputStream()));
	}

	/* Fechar coneccao com o servidor */
	public void close ()
		throws IOException
	{
		this.out.close();
		this.in.close();
		this.cs.close();
	}

	/* Pedido de login*/
	public String login (String username, String password)
		throws IOException
	{
		// mensagem
		this.out.println("login;" + username + ";" + password);

		// resposta
		String resp = this.in.readLine();
		switch (resp) {
			case "OK":
				System.out.println("Esta autenticado");
				break;
			case "ADMIN":
				System.out.println("Esta autenticado como ADMINISTRADOR");
				break;
			case "LOG":
				System.out.println("Utilizador ja logado");
				break;
			default:
				System.out.println("Registo Invalido");
				break;
		}
		return resp;
	}

	/* Pedido de logout*/
	public void logout ()
		throws IOException
	{
		// mensagem
		this.out.println("logout");

		// resposta
		String resp = this.in.readLine();
		if (resp.equals("OK")) {
			System.out.println("Logout efectuado");
		}
	}

	/* Pedido de registo */
	public void register (String username, String password)
		throws IOException
	{
		// mensagem
		this.out.println("reg;" + username + ";" + password);

		// resposta
		String resp = this.in.readLine();
		switch (resp) {
			case "OK":
				System.out.println("Registo efectuado");
				break;
			case "ER":
				System.out.println("Não é o administrador");
				break;
			default:
				System.out.println("Registo ja existente");
				break;
		}
	}

	/* Pedido de submicao do projecto */
	public void submit (String name, String description, String pledged)
		throws IOException
	{
		// mensagem
		this.out.println("sub;" + name + ";" + pledged + ";" + description);

		// resposta
		String resp = this.in.readLine();
		switch (resp) {
			case "OK":
				String id = this.in.readLine();
				System.out.println(id + ": " + name);
				resp = "";
				while (!resp.equals("END")) {
					resp = this.in.readLine();

					// dividir por um espaco
					String[] args = resp.split(" ");

					if (args[0].equals("UP")) {
						System.out.println(args[1] + " -> " + args[2]);
					}
				}
				System.out.println("Projecto Financiado ;)");
				break;
			case "KO":
				System.out.println("Projecto ja existente");
				break;
			case "ER":
				System.out.println("Argumentos invalidos");
				break;
		}
	}

	/* Pedido de financiamento de um projecto */
	public void financing (String cod_proj, String value)
		throws IOException
	{
		// mensagem
		this.out.println("bid;" + cod_proj + ";" + value);

		// resposta
		String resp = this.in.readLine();
		switch (resp) {
			case "OK":
				System.out.println("Transacao efectuada");
				break;
			case "IN":
				System.out.println("Projecto nao existente");
				break;
			case "EX":
				System.out.println("Projecto ja financiado");
				break;
			case "ER":
				System.out.println("Nao tem dinheiro suficiente");
				break;
			default:
				System.out.println("Argumentos invalidos");
				break;
		}
	}

	/* Pedido de listagem de projectos por financiar */
	public void listFinancing (String keyword)
		throws IOException
	{
		// mensagem
		this.out.println("listF;" + keyword);

		// resposta
		String resp = this.in.readLine();
		if (resp.equals("OK")) {
			resp = "";
			while (!resp.equals("END")) {
				resp = this.in.readLine();

				// dividir por um espaco
				String[] args = resp.split(" ");

				if (args[0].equals("LIST")) {
					System.out.println(args[1] + ": " + args[2]);
				}
			}
		} else {
			System.out.println("Nao ha projectos por financiar com esta palavra");
		}
	}

	/* Pedido de listagem de projectos financiados */
	public void listFinanced (String keyword)
		throws IOException
	{
		// mensagem
		this.out.println("listE;" + keyword);

		// resposta
		String resp = this.in.readLine();
		if (resp.equals("OK")) {

			resp = "";
			while (!resp.equals("END")) {
				resp = this.in.readLine();

				// dividir por um espaco
				String[] args = resp.split(" ");

				if (args[0].equals("LIST")) {
					System.out.println(args[1] + ": " + args[2]);
				}
			}
		} else {
			System.out.println("Nao ha projectos ja financiados com esta palavra");
		}
	}

	/* Pedido de informação de um projecto */
	public void projectInfo(String id, String n)
		throws IOException
	{
		// mensagem
		this.out.println("info;" + id + ";" + n);

		// resposta
		String resp = this.in.readLine();

		if (resp.equals("OK")) {

			resp = this.in.readLine();

			// dividir linha pelo caracter ';'
			String[] args = resp.split(";");

			if (args[0].equals("INFO")) {

				String sep = new String(
					new char[args[1].length()]
				).replace("\0", "=");

				System.out.println(
						"\n" + args[1]
						+ "\n" + sep
						+ "\nobtidos " + args[3] + " de " + args[4]
						+ "\n\nDescricao:\n    " + args[2]
						+ "\n"
				);
			}

			// se o valor for zero
			if (n.equals("0")) {
				System.out.println ("Top contribuidores:");
			} else {
				System.out.println ("Top " + n + " contribuidores:");
			}

			resp = "";
			while (!resp.equals("END")) {
				resp = this.in.readLine();

				// dividir linha por espacos
				args = resp.split(" ");

				if (args[0].equals("LIST")) {
					System.out.println(args[1] + " - " + args[2]);
				}
			}
		} else if(resp.equals("ERR")){
			System.out.println("Projecto Inexistente");
		} else {
			System.out.println("Argumentos invalidos");
		}
	}

	/* Pedido para depositar dinheiro */
	public void deposit (String value)
		throws IOException
	{
		// mensagem
		this.out.println("deposit;" + value);

		// resposta
		String resp = this.in.readLine();
		if (resp.equals("OK")) {
			System.out.println("Deposito efectuado");
		} else {
			System.out.println("Operacao nao concluida ");
		}
	}

	/* Pedido do saldo do utilizador */
	public void getSaldo ()
		throws IOException
	{
		// mensagem
		this.out.println("saldo");

		// resposta
		String resp = this.in.readLine();

		if (resp.equals("OK")) {
			resp = this.in.readLine();

			// dividir inha por espaco
			String[] args = resp.split(" ");

			if (args[0].equals("SALDO")) {
				System.out.println("Saldo: " + args[1]);
				System.out.println("Saldo Virtual: " + args[3]);
				System.out.println("Contribuido: " + args[2]);
			}
		} else {
			System.out.println("Operacao nao concluida ");
		}
	}
}
