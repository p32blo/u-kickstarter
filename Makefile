#
# micro-Kickstarter
#
# Trabalho de SD
#
# Elementos:
# Andre Oliveira - a64307
# Diogo Rocha - a64340
# Frederico Mendes - a64331
#
#	MAKEFILE
#

# GLOBAL FLAGS

CLASS_DIR = build
JAR_DIR = dist

CLIENT_MAIN_CLASS = client.Main
SERVER_MAIN_CLASS = server.Main


# targets
.PHONY: all
all: $(CLASS_DIR)/client/Main.class $(CLASS_DIR)/server/Main.class

.PHONY: jar
jar: $(JAR_DIR)/*.jar

$(JAR_DIR)/*.jar: | $(JAR_DIR) $(CLASS_DIR)/client/Main.class $(CLASS_DIR)/server/Main.class
	jar cvfe $(JAR_DIR)/client.jar $(CLIENT_MAIN_CLASS) -C $(CLASS_DIR) client
	jar cvfe $(JAR_DIR)/server.jar $(SERVER_MAIN_CLASS) -C $(CLASS_DIR) server


# compile

$(CLASS_DIR)/client/Main.class: | $(CLASS_DIR) $(wildcard client/commands/*.java) $(wildcard client/*.java)
	javac client/*.java -d $(CLASS_DIR)

$(CLASS_DIR)/server/Main.class: | $(CLASS_DIR) $(wildcard server/*.java)
	javac server/*.java -d $(CLASS_DIR)


# create dirs

$(CLASS_DIR):
	mkdir $@

$(JAR_DIR):
	mkdir $@


# clean targets

.PHONY: clean
clean: clean-class clean-jar

.PHONY: clean-class
clean-class:
	rm -rf $(CLASS_DIR)
.PHONY: clean-jar
clean-jar:
	rm -rf $(JAR_DIR)
