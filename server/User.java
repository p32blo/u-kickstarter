/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package server;

/*
 * User
 */
public class User
{
	private String name;
	private String password;

	private double saldoActual;
	private double contributed;


	public User (String name, String password)
	{
		this.name = name;
		this.password = password;

		// saldo inicializado a 0
		this.contributed = 0.0;
		this.saldoActual = 0.0;
	}

	/* Getters */
	public String getName () {return this.name;}
	public String getPassword () {return this.password;}

	public double getContributed() {return this.contributed;}
	public double getSaldoActual () {return this.saldoActual;}
	public double getSaldoVirtual () {return this.saldoActual - this.contributed;}

	/* Fazer um deposito na conta do utilizador */
	public synchronized void deposit (double value)
		throws MindFuckException
	{
		// nao permetir retirar um valor superior ao contribuido
		if (-value > this.getSaldoVirtual()){
			throw new MindFuckException ();
		}
		this.saldoActual += value;
	}

	/* Financiar um projecto */
	public synchronized void finance (double value)
			throws NoMoneyException, IllegalArgumentException
	{
		if (value <= 0) {
			throw new IllegalArgumentException ("VAlue must be positive");
		}

		if (value > this.getSaldoVirtual()) {
			throw new NoMoneyException ();
		}

		this.contributed += value;
	}

	/* Efectuar a transacao da contribuicao */
	public synchronized void transaction (double value)
		throws NoMoneyException
	{
		if (value > this.getSaldoActual()) {
			throw new NoMoneyException ();
		}

		this.saldoActual -= value;
		this.contributed -= value;
	}
}

/* Custom Exceptions */
class NoMoneyException extends Exception {}
class MindFuckException extends Exception {}
