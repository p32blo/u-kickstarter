/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package server;

import java.io.IOException;

/*
 * Main
 */
public class Main
{
	public static void main(String[] args)
	{
		try {
			Server.start();
		} catch (IOException e) {
			System.out.println("Server Already Running");
			System.out.println("- exit -");
		}
	}
}
