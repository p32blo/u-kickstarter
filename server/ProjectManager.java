/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package server;

import java.util.Collections;

import java.util.Map;
import java.util.HashMap;

import java.util.List;
import java.util.ArrayList;

/*
 * ProjectManager
 */
public class ProjectManager
{
	// atribuidor de id's
	static int i = 1;

	// id -> Project
	Map<Integer, Project> projects;

	public ProjectManager ()
	{
		this.projects = new HashMap<Integer, Project> ();
	}

	/* adicionar projecto */
	public synchronized Project addProject (String name, String description, double pledged)
	{
		Project proj = new Project (i, name, description, pledged);
		this.projects.put(i, proj);
		i++;
		return proj;
	}

	/* Lista de projectos financiados */
	List<Project> getFinanced ()
	{
		List<Project> res = new ArrayList<Project> ();

		for (Project p : this.projects.values()) {
			if (p.isFinanced()) {
				res.add(p);
			}
		}
		return res;
	}

	/* Lista de projectos por financiar */
	List<Project> getNotFinanced ()
	{
		List<Project> res = new ArrayList<Project> ();

		for (Project p : this.projects.values()) {
			if (!p.isFinanced()) {
				res.add(p);
			}
		}
		return res;
	}

	/* List de projectos financiados que contem uma dada keyword */
	List<Project> searchFinanced (String keyword)
	{
		List<Project> res = new ArrayList<Project> ();

		for (Project p : this.getFinanced()) {
			if (p.getDescription().contains(keyword)) {
				res.add(p);
			}
		}
		return res;
	}

	/* List de projectos por financiar que contem uma dada keyword */
	List<Project> searchNotFinanced(String keyword)
	{
		List<Project> res = new ArrayList<Project> ();

		for (Project p : this.getNotFinanced()) {
			if (p.getDescription().contains(keyword)) {
				res.add(p);
			}
		}
		return res;
	}

	/* Project by ID */
	public Project get (int id)
			throws InexistentProjectException
	{
		Project proj =  this.projects.get(id);
		if (proj == null) {
			throw new InexistentProjectException ();
		}
		return proj;
	}

	/* verifica a existencia de um projeeccto */
	public boolean hasProject (String name)
	{
		for (Project p : this.projects.values()) {
			if (name.equals(p.getName())) {
				return true;
			}
		}
		return false;
	}

	/* Adicionar um pojecto */
	public Project submitProject (String name, String description, double pledged)
		throws ExistentProjectException, IllegalArgumentException
	{
		if (this.hasProject(name)) {
			throw new ExistentProjectException ();
		}

		if (pledged <= 0) {
			throw new IllegalArgumentException ("Value must be positive.");
		}

		Project proj = this.addProject(name, description, pledged);
		return proj;
	}

	/* Devolver lista dos n utilizadares mais contribuintes num dado projecto */
	List<Transaction> projectInfo (int proj_id, int n)
		throws IllegalArgumentException
	{
		if (n < 0) {
			throw new IllegalArgumentException ();
		}

		Project proj = this.projects.get(proj_id);

		// Reunir todoas as transacoes de um dado utilizador

		Map<String, Double> aux = new HashMap<> ();

		for (Transaction trans : proj.getContributions()) {

			String usr = trans.getUsername();
			double val = trans.getValue();

			if (!aux.containsKey(usr)) {
				aux.put(usr, 0.0);
			}

			aux.put(usr, aux.get(usr) + val);
		}

		// Nova lista

		List<Transaction> res = new ArrayList<> ();

		for (Map.Entry<String,Double> usr : aux.entrySet()) {
			res.add(new Transaction (usr.getKey(), usr.getValue()));
		}

		//ordenar
		Collections.sort(res);

		// retirar os n primeiros elementos da lista
		if (n > 0 && n < res.size()) {
			res = res.subList(0, n);
		}

		return res;
	}
}

/* Custom Exception */
class ExistentProjectException extends Exception {}
class InexistentProjectException extends Exception {}
