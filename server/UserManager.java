/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package server;

import java.util.Map;
import java.util.HashMap;

/*
 * UserManager
 */
public class UserManager
{
	// username -> Users
	Map<String, User> users;

	public UserManager ()
	{
		this.users = new HashMap<String, User> ();

		// Utilizador predefinidos
		try {
			this.register("admin", "123");
			this.register("test", "123");
		}catch (UserFoundException e) {
			//pass
		}
	}

	/* Validar o login de um utilizador*/
	public void login (String username, String password)
		throws UserNotFoundException, AuthenticationFailedException
	{
		User user = this.users.get(username);

		if (user == null) {
			throw new UserNotFoundException ();
		}

		if (!password.equals(user.getPassword())) {
			throw new AuthenticationFailedException();
		}
	}

	/* Registar novo utilizador */
	public synchronized void register (String username, String password)
		throws UserFoundException
	{
		if (this.users.containsKey(username)) {
			throw new UserFoundException();
		}

		this.users.put(username, new User (username, password));
	}

	/* remover utilizador */
	public synchronized void unRegister (String username, String pass)
		throws UserNotFoundException
	{
		if (!this.users.containsKey(username)) {
			throw new UserNotFoundException();
		}

		this.users.remove(username);
	}

	/* Devolve utilizador com o username */
	public User get (String username)
		throws UserNotFoundException
	{
		User user =  this.users.get(username);

		if (user == null) {
			throw new UserNotFoundException ();
		}

		return user;
	}
}

/* Custom Exception */
class UserNotFoundException extends Exception {}
class AuthenticationFailedException extends Exception {}
class UserFoundException extends Exception {}
