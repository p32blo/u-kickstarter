/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package server;

import java.net.Socket;
import java.net.ServerSocket;

import java.io.BufferedReader;
import java.io.PrintWriter;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.io.IOException;

import java.util.List;


/*
 * Server
 */
public class Server
{
	static int i = 1;

	// Variaveis que sao partilhadas por todas as threads
	private static UserManager users = new UserManager ();
	private static ProjectManager projects = new ProjectManager ();

	/* Start the server */
	public static void start ()
		throws IOException
	{
		ServerSocket server = new ServerSocket (9999);
		Socket client;

		System.out.println("- server started -");

		while (true) {
			client = server.accept();

			System.out.println("New Socket: " + i);
			i++;

			// nova thread por cada conexao aberta
			new Thread (
					new SocketServer (client, users, projects)
			).start();
		}
	}
}

/*
 * SocketServer
 */
class SocketServer implements Runnable
{
	private Socket client;

	// Estado partilhado
	private UserManager users;
	private ProjectManager projects;

	// Socket IO
	BufferedReader reader;
	PrintWriter writer;

	// Nome do user logado ou vazia se nao estiver ninguem*/
	private String username;

	/*Verificar se e o admin quem esta logado*/
	private boolean isAdmin () {
		return this.username.equals("admin");
	}

	public SocketServer (Socket client, UserManager users, ProjectManager projects)
	{
		this.client = client;

		this.users = users;
		this.projects = projects;

		// nao logado
		this.username = "";

		try {
			this.reader = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
			// auto flush
			this.writer = new PrintWriter(new OutputStreamWriter(this.client.getOutputStream()), true);
		} catch (IOException e) {
			System.out.println("Cannot estalbish connection");
		}
	}

	/* Reposta ao login*/
	public void login (String username, String password)
	{
		try {
			this.users.login(username, password);

			this.username = username;

			if (this.isAdmin()) {
				writer.println("ADMIN");
			} else {
				writer.println("OK");
			}
		} catch (AuthenticationFailedException | UserNotFoundException e) {
			writer.println("KO");
		}
	}
	/* Reposta ao registar*/
	public void register (String username, String password)
	{
		try {
			if (this.isAdmin()) {
				this.users.register(username, password);
				writer.println("OK");
			} else  {
				writer.println("ER");
			}
		} catch (UserFoundException e) {
			writer.println("KO");
		}
	}
	/* Reposta ao submeter projecto*/
	public void submit (String name, String description, String pledged)
	{
		try {
			double value = Double.parseDouble(pledged);

			Project proj = this.projects.submitProject(name, description, value);

			this.writer.println("OK");
			this.writer.println(proj.getID());

			proj.watch(this.writer, this.users);
		} catch (ExistentProjectException e) {
			this.writer.println("KO");
		} catch (IllegalArgumentException e) {
			this.writer.println("ER");
		}
	}
	/* Reposta ao financiamento de um projecto*/
	public void financing (String cod_proj, String value)
	{
		try {
			int id = Integer.parseInt(cod_proj);
			double val = Double.parseDouble(value);

			User user = this.users.get(this.username);
			Project proj = this.projects.get(id);


			if (!proj.isFinanced()) {
				// tem que estar por esta ordem  porque se o user
				// nao tiver dinheiro es nao pode efectuar a trasacao
				user.finance(val);
				proj.finance(this.username, val);

				writer.println("OK");
			} else {
				writer.println("EX");
			}

		} catch (InexistentProjectException e) {
			writer.println("IN");
		} catch (ProjectFinancedException e) {
			writer.println("EX");
		} catch (NoMoneyException e) {
			writer.println("ER");
		} catch (IllegalArgumentException | UserNotFoundException e) {
			writer.println("KO");
		}
	}

	/* Reposta a listagem de projectos por financiar */
	public void listFinancing (String keyword)
	{
		List<Project> res = this.projects.searchNotFinanced(keyword);

		if (res.isEmpty()) {
			writer.println("KO");
		} else {
			writer.println("OK");

			for (Project proj : res) {
				writer.println("LIST " + proj.getID() + " " + proj.getName());
			}
			writer.println("END");
		}
	}

	/* Reposta a listagem de projectos ja financiados */
	public void listFinanced (String keyword)
	{
		List<Project> aux = this.projects.searchFinanced(keyword);
		if (aux.isEmpty()) {
			writer.println("KO");
		} else {
			writer.println("OK");

			for (Project proj : aux) {
				writer.println("LIST " + proj.getID() + " " + proj.getName());
			}
			writer.println("END");
		}
	}

	/* Reposta a informacao do porjecto */
	public void projectInfo(String id, String num)
	{
		try {
			int proj_id = Integer.parseInt(id);
			int n = Integer.parseInt(num);

			Project proj = this.projects.get(proj_id);

			// Verificao antes de emviar qualquer informacao
			if (n >= 0) {
				writer.println("OK");

				writer.println("INFO;" + proj.getName() + ";" + proj.getDescription() + ";" + proj.getRaised() + ";" + proj.getPledged());

				for (Transaction trans : this.projects.projectInfo(proj_id, n)) {
					writer.println("LIST " + trans.getUsername() + " " + trans.getValue());
				}
				writer.println("END");
			} else {
				writer.println("KO");
			}
		} catch (InexistentProjectException e) {
			writer.println("ERR");
		} catch (IllegalArgumentException e) {
			writer.println("KO");
		}
	}

	/* Reposta ao deposito de utiizador */
	public void deposit (String value)
	{
		try {
			double val = Double.parseDouble(value);

			User user = this.users.get(this.username);
			user.deposit(val);

			this.writer.println("OK");
		} catch (NumberFormatException | MindFuckException | UserNotFoundException e) {
			this.writer.println("KO");
		}
	}

	/* Reposta ao pedido de consulta do saldo*/
	public void saldo ()
	{
		try {
			User user = this.users.get(this.username);
			this.writer.println("OK");
			this.writer.println("SALDO " + user.getSaldoActual() + " " + user.getContributed() + " " + user.getSaldoVirtual());
		} catch (UserNotFoundException ex) {
			this.writer.println("KO");
		}
	}

	/*
	 * Main loop
	 */
	@Override
	public void run ()
	{
		try {
			String line = "";

			while (line != null) {

				line = reader.readLine();

				if (line == null) {
					System.out.println("Conection Lost");
					break;
				}

				// Se nao tiver ';' na fazer nada
				if (line.isEmpty()) {
					continue;
				}

				// dividir no caracter ';'
				String[] args = line.split(";");
				String cmd_name = args[0];

				//Login esta sempre disponivel
				if (cmd_name.equals("login")) {
					this.login(args[1], args[2]);
				}

				//So permitir o resto se tiver login feito
				if (!this.username.isEmpty()) {
					switch (cmd_name) {
						case "logout":
							this.username = "";
							writer.println("OK");
							break;
						case "reg":
							this.register(args[1], args[2]);
							break;
						case "sub":
							this.submit(args[1], args[3], args[2]);
							break;
						case "bid":
							this.financing(args[1], args[2]);
							break;
						case "listF":
							this.listFinancing(args[1]);
							break;
						case "listE":
							this.listFinanced(args[1]);
							break;
						case "info":
							this.projectInfo(args[1], args[2]);
							break;
						case "deposit":
							this.deposit(args[1]);
							break;
						case "saldo":
							this.saldo();
							break;
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Conection Lost");
		}
	}
}
