/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - 64331
 */

package server;

/*
 * Transaction
 */
public class Transaction implements Comparable<Transaction>
{
	private String username;
	private double value;

	/* Constructores */

	public Transaction (String username, double value)
	{
		this.username = username;
		this.value = value;
	}

	public Transaction (Transaction t) {
		this.username = t.getUsername();
		this.value = t.getValue();
	}

	/* Getters */
	public String getUsername () {return this.username;}
	public double getValue () {return this.value;}

	@Override
	public Transaction clone () {
		return new Transaction (this);
	}

	/* Para ordenar instancias desta classe */
	@Override
	public int compareTo (Transaction b) {
		int res = Double.compare(b.getValue(), this.value);

		if (res == 0) {
			res = this.username.compareTo(b.getUsername());
		}
		return res;
	}
}
