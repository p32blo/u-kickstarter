/*
 * micro-Kickstarter
 *
 * Trabalho de SD
 *
 * Elementos:
 * Andre Oliveira - a64307
 * Diogo Rocha - a64340
 * Frederico Mendes - a64331
 */

package server;

import java.util.List;
import java.util.ArrayList;

import java.io.PrintWriter;

/*
 * Project
 */
public class Project
{
	private int id;

	private String name;
	private String description;

	// valor de finaciamento requerido
	private final double pledged;
	// valor acumulado ate ao momento
	private double raised;

	// Lista de todas as contribuicoes efectuadas no projecto
	private List<Transaction> contributions;


	public Project (int id, String name, String description, double pledged)
	{
		this.name = name;
		this.description = description;
		this.pledged = pledged;

		this.id = id;

		this.raised = 0;
		this.contributions = new ArrayList<Transaction> ();
	}

	/* Getters */
	public String getName () {return this.name;}
	public String getDescription () {return this.description;}
	public double getPledged () {return this.pledged;}
	public double getRaised () {return this.raised;}
	public int getID () {return this.id;}
	public List<Transaction> getContributions ()
	{
		List<Transaction> res = new ArrayList<Transaction> ();
		for (Transaction trans : this.contributions) {
			res.add(trans.clone());
		}
		return res;
	}

	/* Loop para enviar atualizavces das contribuicoes do projecto*/
	public synchronized void watch (PrintWriter writer, UserManager users)
	{

		// esperar ate o projecto estar finaciado
		for (int index = 0; !this.isFinanced(); index++) {

			while (this.contributions.size() <= index) {
				try {
					wait();
				} catch (InterruptedException e) {
					//pass
				}
			}

			writer.println("UP " + this.contributions.get(index).getUsername() + " " + this.contributions.get(index).getValue());
			writer.flush();
		}

		// efetuar o pagamento de todos os utilizadores envolvidos
		for (Transaction trans : this.contributions) {
			try {
				users.get(trans.getUsername()).transaction(trans.getValue());
			} catch (UserNotFoundException | NoMoneyException ex) {
				//pass - nao deve ser possivel ocorrer
			}
		}

		writer.println("END");
		writer.flush();
	}

	/* Financiar o projecto */
	public synchronized Transaction finance (String username, double value)
		throws IllegalArgumentException, ProjectFinancedException
	{
		if (value <= 0) {
			throw new IllegalArgumentException ("value must be positive.");
		}

		if (this.isFinanced()) {
			throw new ProjectFinancedException ();
		}

		Transaction trans = new Transaction (username, value);
		this.contributions.add(trans);
		notify(); // see this.whatch()

		this.raised += trans.getValue();

		return trans;
	}

	public boolean isFinanced () {
		return (this.raised >= this.pledged);
	}

	@Override
	public String toString () {
		return this.id + ": " + this.name;
	}
}

/* Custom Exceptions */
class ProjectFinancedException extends Exception {}
